## my-renren-fast-vue
前端系统启动步骤

```sh
# node 和 npm 版本
# node：v14.21.3
# npm：6.14.18

# 全局安装
sudo npm install -g webpack
sudo npm install -g @vue/cli-init

# npm 设置
sudo npm config set registry http://registry.npm.taobao.org/

# 首次启动先安装node-sass
sudo npm install node-sass@npm:sass --ignore-scripts

# 再进行初始化项目
sudo npm install

# 每次启动运行项目
sudo npm run dev
```

